<?php

namespace PecqueurS\LaravelHelpers\Json;

use stdClass;
use Illuminate\Support\Collection;

class Json
{
    public function __construct(
        protected mixed $data, 
        protected $jsonFlags = JSON_UNESCAPED_UNICODE
    ) {}

    public static function of(mixed $data, $jsonFlags = JSON_UNESCAPED_UNICODE)
    {
        return new static($data, $jsonFlags);
    }

    public function toObject($forceObjectType = false, $jsonFlags = null): mixed
    {
        if ($this->data instanceof stdClass) {
            return $this->data;
        }

        $result = json_decode($this->toString(), false, 512, $jsonFlags ?? $this->jsonFlags);

        return $forceObjectType ? (object) $result : $result;
    }

    public function toArray($jsonFlags = null): ?array
    {
        if (is_array($this->data)) {
            return $this->data;
        }

        return json_decode($this->toString(), true, 512, $jsonFlags ?? $this->jsonFlags);
    }

    public function toString($jsonFlags = null): string
    {
        if (is_string($this->data)) {
            return $this->data;
        }

        return json_encode($this->data, $jsonFlags ?? $this->jsonFlags);
    }

    public function toCollection($jsonFlags = null): Collection
    {
        return collect($this->toArray($jsonFlags));
    }
}

# Laravel Helpers JSON

## objective

helper to facilitate json/array/string conversion.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-helpers-json
```

### Usage

```php
use PecqueurS\LaravelHelpers\Json\Json;

...

// set
$dataFromString = Json::of('{"test": 1}');
$dataFromArray = Json::of(["test" => 1]);
$dataFromCollection = Json::of(collect(["test" => 1]));
$dataFromObject = Json::of(new stdClass());

// Output
$dataFromString->toString(JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
$dataFromString->toArray();
$dataFromString->toObject();
$dataFromString->toCollection();

// Thrown
try {
    Json::of('{"test": 1', JSON_UNESCAPED_UNICODE|JSON_THROW_ON_ERROR)->toArray(JSON_UNESCAPED_UNICODE);
} catch (\Throwable $e) {
    // not valid json
}

```
